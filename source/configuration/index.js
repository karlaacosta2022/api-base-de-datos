// Core de NodeJS
const express = require('express')

// Terceros
const cors = require('cors')
const morgan = require('morgan')

// Opciones de cors
let corsOpciones = {
  origin: '*',
  optionsSuccessStatus: 200
}

// Rutas
const {
  ventaRoutes,
  compraRoutes,
  vendedorRoutes,
  clienteRoutes
} = require('../routes')

// App
const app = express()

// Router
const router = express.Router()

app
  .use(express.json())
  .use(cors(corsOpciones))
  .use(morgan('dev'))

router.use('/venta', ventaRoutes)
router.use('/compra', compraRoutes)
router.use('/vendedor', vendedorRoutes)
router.use('/cliente', clienteRoutes)
app.use(router)

module.exports = { app }