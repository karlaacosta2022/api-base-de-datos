const { pool } = require('../database')
const {executeTransaction} = require('../transactions')

const transaccionDeVenta = async (req, res) => {
  const {ven_id, cli_id, farmaco_id, cantidad} = req.body
  const INACTIVO = 2

  try {
    executeTransaction(async (pool) => {

      let estadoVendedor = await pool.query(
        'SELECT estado_id FROM vendedor WHERE ven_id = $1', 
        [ven_id]
      )
      estadoVendedor = estadoVendedor.rows[0].estado_id

      if(estadoVendedor == INACTIVO){
        res.json({ notificacion: 'Este vendedor se encuentra inactivo' })
      }

      let cantidadFarmaco = await pool.query(
        'SELECT farmaco_stock FROM farmaco WHERE FARMACO_ID = $1', 
        [farmaco_id]
      )
      cantidadFarmaco = cantidadFarmaco.rows[0].farmaco_stock

      if(cantidadFarmaco < cantidad){
        res.json({ notificacion: 'No existe suficiente stock' })
      } else {
        const venta_id = Math.ceil(Math.random() * 1000)
        const detav_id = Math.ceil(Math.random() * 1000)
        const restaFarmaco = cantidadFarmaco - Number(cantidad)
        await pool.query(
          'UPDATE farmaco SET farmaco_stock = $1 WHERE farmaco_id = $2',
          [restaFarmaco, farmaco_id,]
        )
        await pool.query(
          'INSERT INTO venta VALUES($1, $2, $3)',
          [venta_id, new Date(), null]
        )
        await pool.query(
          'INSERT INTO detalleventa VALUES($1, $2, $3, $4, $5, $6)',
          [detav_id, venta_id, cli_id, farmaco_id, ven_id, cantidad]
        )
        res.json({ notificacion: 'Proceso finalizado correctamente' })
      }
    })
  } catch (e) {
    res.json(e.message)
  }
}

const movimientosDeVenta = async (req, res) => {
  try {
    const movimientos = await pool.query(
      'SELECT cliente.cli_id, cli_cedula, cli_nombres, detav_cantidad, '+
      'farmaco.farmaco_id, farmaco_nombre, venta_fecha, ven_nombres, vendedor.ven_id '+
      'FROM cliente '+
      'INNER JOIN detalleventa ON cliente.cli_id = detalleventa.cli_id '+
      'INNER JOIN farmaco ON detalleventa.farmaco_id = farmaco.farmaco_id '+
      'INNER JOIN venta ON detalleventa.venta_id = venta.venta_id '+
      'INNER JOIN vendedor ON detalleventa.ven_id = vendedor.ven_id'
    )
    res.json(movimientos.rows)
  } catch (e) {
    res.json(e.message)
  }
}

module.exports = {
  transaccionDeVenta,
  movimientosDeVenta
}
