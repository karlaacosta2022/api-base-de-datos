const { pool } = require('../database')

const readVendedores = async (req, res) => {
  try {
    const resultado = await pool.query('SELECT * FROM vendedor')
    res.json(resultado.rows)
  } catch (error) {
    res.json(error.message)
  }
}

const createVendedor = async (req, res) => {
  const { ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_fechanacimiento,
    ven_fechaingreso, ven_supervisor } = req.body
  try {
    const resultado = await pool.query(
      'INSERT INTO vendedor VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *', 
      [ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_fechanacimiento,
      ven_fechaingreso, ven_supervisor]
    )
    res.json(resultado.rows[0])
  } catch (error) {
    res.json(error.message)
  }
}

const updateVendedor = async (req, res) => {
  const { id } = req.params
  const { ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_apellidos, ven_fechanacimiento,
    ven_fechaingreso, ven_supervisor } = req.body
  try {
    const resultado = await pool.query(
      'SELECT * FROM vendedor WHERE ven_id = $1', 
      [id]
    )

    if(resultado.rows.length === 0){
      await pool.query(
        'INSERT INTO vendedor VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9)', 
        [ven_id, suc_id, estado_id, horario_id, ven_nombres, ven_apellidos, new Date(ven_fechanacimiento),
        new Date(ven_fechaingreso), ven_supervisor]
      )
      res.json({ notificacion: 'Registro insertado' })
    } else {
      await pool.query(
        'UPDATE vendedor SET suc_id = $1, estado_id = $2, horario_id = $3, ven_supervisor = $4 WHERE ven_id = $5',
        [suc_id, estado_id, horario_id, ven_supervisor, id ]
      )
      res.json({ notificacion: 'Registro modificado' })
    }
  } catch (error) {
    res.json(error.message)
  }
}

const deleteVendedor = async (req, res) => {
  const { id } = req.params
  try {
    const resultado = await pool.query(
      'SELECT * FROM vendedor WHERE ven_id = $1', 
      [id]
    )

    if(resultado.rows.length === 0){
      return res.status(404).json({
        notificacion: 'Vendedor no encontrado'
      })
    } else {
      await pool.query(
        'DELETE FROM vendedor WHERE ven_id = $1',
        [id]
      )
      res.json({ notificacion: 'Eliminación con éxito' })
    }
  } catch (error) {
    res.json(error.message)
  }
}

module.exports = {
  createVendedor,
  readVendedores,
  updateVendedor,
  deleteVendedor
}
