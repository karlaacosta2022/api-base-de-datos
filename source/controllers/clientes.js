const { pool } = require('../database')

const readClientes = async (req, res) => {
  try {
    const resultado = await pool.query('SELECT * FROM cliente')
    res.json(resultado.rows)
  } catch (error) {
    res.json(error.message)
  }
}

const createCliente = async (req, res) => {
  const { cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
    cli_email, cli_fechanacimiento, cli_prescripcion_ } = req.body
  try {
    const resultado = await pool.query(
      'INSERT INTO cliente VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *', 
      [cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
        cli_email, cli_fechanacimiento, cli_prescripcion_]
    )
    res.json(resultado.rows[0])
  } catch (error) {
    res.json(error.message)
  }
}

const updateCliente = async (req, res) => {
  const { id } = req.params
  const { cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
    cli_email, cli_fechanacimiento, cli_prescripcion_ } = req.body
  try {
    const resultado = await pool.query(
      'SELECT * FROM cliente WHERE cli_id = $1', 
      [id]
    )

    if(resultado.rows.length === 0){
      console.log(req.body)
      const resultado = await pool.query(
        'INSERT INTO cliente VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *', 
        [cli_id, cli_cedula, cli_nombres, cli_apellidos, cli_direccion, cli_telefono,
          cli_email, new Date(cli_fechanacimiento), cli_prescripcion_]
      )
      res.json({ notificacion: 'Registro insertado' })
    } else {
      await pool.query(
        'UPDATE cliente SET cli_direccion = $1, cli_telefono = $2, cli_email = $3, cli_prescripcion_ = $4 WHERE cli_id = $5',
        [cli_direccion, cli_telefono, cli_email, cli_prescripcion_, id ]
      )
      res.json({ notificacion: 'Registro modificado' })
    }
  } catch (error) {
    res.json(error.message)
  }
}

const deleteCliente = async (req, res) => {
  const { id } = req.params
  try {
    const resultado = await pool.query(
      'SELECT * FROM cliente WHERE cli_id = $1', 
      [id]
    )

    if(resultado.rows.length === 0){
      return res.status(404).json({
        notificacion: 'Vendedor no encontrado'
      })
    } else {
      await pool.query(
        'DELETE FROM cliente WHERE cli_id = $1',
        [id]
      )
      res.json({ notificacion: 'Eliminación con éxito' })
    }
  } catch (error) {
    res.json(error.message)
  }
}

module.exports = {
  createCliente,
  readClientes,
  updateCliente,
  deleteCliente
}
