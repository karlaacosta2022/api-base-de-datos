const { pool } = require('../database')
const {executeTransaction} = require('../transactions')

const transaccionDeCompra = async (req, res) => {
  const {prov_id, farmaco_id, cantidad} = req.body
  const NO_EXISTE_FARMACO = 0

  try {
    executeTransaction(async (pool) => {

      let farmaco = await pool.query(
        'SELECT * FROM farmaco WHERE farmaco_id = $1', 
        [farmaco_id]
      )

      const precio = farmaco.rows[0].farmaco_precio
      const stock = farmaco.rows[0].farmaco_stock

      if(farmaco.rows.length === NO_EXISTE_FARMACO){
        res.json({ notificacion: 'No existe el farmaco indicado' })
      } else {
        const adqui_id = Math.ceil(Math.random() * 1000)
        
        await pool.query(
          'INSERT INTO adquisicionfarmaco VALUES($1, $2, $3, $4, $5)',
          [adqui_id, farmaco_id, prov_id, new Date(), (cantidad * precio)]
        )
        await pool.query(
          'UPDATE farmaco SET farmaco_stock = $1 WHERE farmaco_id = $2',
          [stock + Number(cantidad), farmaco_id]
        )
        res.json({ notificacion: 'Proceso finalizado correctamente' })
      }
    })
  } catch (e) {
    res.json(e.message)
  }
}

const movimientosDeCompra = async (req, res) => {
  try {
    const movimientos = await pool.query(
      'SELECT farmaco.farmaco_id, prov_nombre, adqui_fecha, adqui_costo, '+
      'farmaco_nombre, farmaco_precio, farmaco_stock, proveedor.prov_id '+
      'FROM proveedor '+
      'INNER JOIN adquisicionfarmaco ON proveedor.prov_id = adquisicionfarmaco.prov_id '+
      'INNER JOIN farmaco ON adquisicionfarmaco.farmaco_id = farmaco.farmaco_id '
    )
    res.json(movimientos.rows)
  } catch (e) {
    res.json(e.message)
  }
}

module.exports = {
  transaccionDeCompra,
  movimientosDeCompra
}