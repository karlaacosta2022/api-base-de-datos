const { Router } = require('express')
const router = Router()

const {
  createCliente,
  readClientes,
  updateCliente,
  deleteCliente
} = require('../controllers/clientes')

router.get('/', readClientes)

router.post('/', createCliente)

router.put('/:id', updateCliente)

router.delete('/:id', deleteCliente)

module.exports = router