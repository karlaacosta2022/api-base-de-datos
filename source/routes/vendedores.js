const { Router } = require('express')
const router = Router()

const {
  createVendedor,
  readVendedores,
  updateVendedor,
  deleteVendedor
} = require('../controllers/vendedores')

router.get('/', readVendedores)

router.post('/', createVendedor)

router.put('/:id', updateVendedor)

router.delete('/:id', deleteVendedor)

module.exports = router