const { Router } = require('express')
const router = Router()

const {
  transaccionDeVenta,
  movimientosDeVenta
} = require('../controllers/venta')

router.get('/', movimientosDeVenta)

router.post('/', transaccionDeVenta)

module.exports = router