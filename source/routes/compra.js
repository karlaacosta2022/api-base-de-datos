const { Router } = require('express')
const router = Router()

const {
  transaccionDeCompra,
  movimientosDeCompra
} = require('../controllers/compra')

router.get('/', movimientosDeCompra)

router.post('/', transaccionDeCompra)

module.exports = router