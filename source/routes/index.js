module.exports = {
  ventaRoutes: require('./venta'),
  compraRoutes: require('./compra'),
  vendedorRoutes: require('./vendedores'),
  clienteRoutes: require('./clientes')
}