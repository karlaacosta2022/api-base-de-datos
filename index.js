const { app } = require('./source/configuration')
const PORT = 5000

// Servidor
app.listen(PORT, () => {
  console.log(`Servidor en el puerto: ${PORT}`)
})